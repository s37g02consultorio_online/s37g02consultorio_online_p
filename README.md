Instrucciones de la línea de comandos

También puede cargar un archivo existente para su computadora usando las instrucciones a continuación 

Configuración global de Git

git config --global user.name "MIGUEL ANTONIO NIETO USTARIZ"
git config --global user.email "miguel-solid@hotmail.com"

Crear un nuevo repositorio

git clone https://gitlab.com/s37g02consultorio_online/s37g02consultorio_online_p.git
cd s37g02consultorio_online_p
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

Push a una carpeta existente

cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/s37g02consultorio_online/s37g02consultorio_online_p.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push a un repositorio de Git existente
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/s37g02consultorio_online/s37g02consultorio_online_p.git
git push -u origin --all
git push -u origin --tags


Agregar contenido...
Agregar contenido...
Agregar contenido...

Agregar contenido por developer...
Agregar contenido por developer...
Agregar contenido por developer...
